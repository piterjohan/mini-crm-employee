<?php

namespace Tests\Feature;

use App\Employee;
use App\Models\CompaniesModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    const SUBDOMAIN = 'mini-crm.com';

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }


    /**
     * get first url
     * @return string company_name
     */
    protected function getFirstUrl($company_name)
    {
        $url = "http://" . $company_name . ".mini-crm.com";
        $current_url = explode('.', parse_url($url, PHP_URL_HOST));

        return strtolower($current_url[0]);
    }

    public function createCompany($name = null)
    {
        // company
        $company = factory(CompaniesModel::class)->create(['name' => $name]);
        return $company;
    }

    public function logged()
    {
        $company = $this->createCompany('asus');
        // first url
        $url_company = $this->getFirstUrl($company->name);
        // create employee
        $user = factory(Employee::class)->create([
            'id_company' => $company->id_company,
            'password' => Hash::make("123"),
        ]);
        

        $this->actingAs($user)
            ->withSession(['jwt_data' => $user])
            ->withSession([
                'domain_data' => [
                    'url_company' => $url_company,
                    'company_name' => strtolower($company->name),
                    'id_company' => $company->id_company,

                ]
            ]);
    }

    /**
     * @test
     */
    public function user_can_see_login()
    {
        // get name company
        $company = $this->createCompany('asus');
        $url = "http://" . strtolower($company->name). "." . LoginTest::SUBDOMAIN . "/login";
        
        $response = $this->get($url);
        $response->assertStatus(200);

        // $response->dump();
        // $response->dumpSession();
        // dd($url);
    }

    /**
     * @test
     */
    public function can_see_list_employee_if_login()
    {
        $this->withoutExceptionHandling();
        $this->logged();
        
        $url = "http://" . Session::get('domain_data')['url_company']. "." . LoginTest::SUBDOMAIN . "/list-employee";
        
        // check value
        $email = Session::get('jwt_data')->email;
        
        $response = $this->get($url);
        $response->assertStatus(200);
        $response->assertSee($email, true);

        // $response->dump();
        // $response->dumpSession();
        // dd($url, $email);
    }

}
