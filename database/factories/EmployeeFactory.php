<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'id_company' => null,
        'email' => $faker->safeEmail,
        'password' => null,
        'phone' => $faker->e164PhoneNumber,
        'created_at' => Carbon::now(),
    ];
});
