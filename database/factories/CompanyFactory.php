<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CompaniesModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(CompaniesModel::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->companyEmail,
        'logo' => $faker->imageUrl,
        'created_at' => Carbon::now(),
    ];
});
