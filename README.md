# How to Use

1. This is the continue project of
[Mini-Crm](https://gitlab.com/piterjohan/mini-crm) better migrate and seed from there first **(Recommend)**

2. clone or download this project
> https://gitlab.com/piterjohan/mini-crm-employee.git

3. > composer install

4. Auth Laravel
```
composer require laravel/ui:^2.4

php artisan ui bootstrap --auth

npm install & npm run dev
```
5. copy .env.example to .env

6. edit file .env like below
```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT= your port
    DB_DATABASE= your databasename
    DB_USERNAME= your username
    DB_PASSWORD= your password
```
7. Migrate database
> php artisan:migrate

8.The application ready to go