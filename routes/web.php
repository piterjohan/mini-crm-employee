<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::domain('{company}.mini-crm.com')->group(function () {
    Auth::routes(['register' => false, 'reset' => false]);
});

// auth
Route::group([
    'middleware' => ['auth', 'checkJWT', 'checkCurrentDomain']
], function () {
    Route::domain('{company}.mini-crm.com')->group(function () {
        
        Route::get('list-employee','DataController@listEmployee')->name('data.listemployee');
    });
});
