@extends('layouts.loginTemplate')
@section('title', 'Login Page')

@section('customCss')

    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    {{-- flag icon --}}
    <link rel="stylesheet" href="{{ asset ('flag-icons-master/css/flag-icon.css') }}">
@endsection

@section('content')
    <body class="login-page">
        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);">Mini <b> CRM</b></a>
            </div>
            <div class="card">
                <div class="body">
                    <form action="{{ route('login', [$current_url] )}}" id="sign_in" method="POST">
                        @csrf
                        <div class="msg">Login</div>
        
                        @if( Session::get('force_logout') )
                            <div class="alert alert-warning alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('force_logout')}}
                            </div> 
                        @endif

                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                @error('email')
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ $message }}
                                    </div>  
                                @enderror

                                <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                    @error('password')
                                        <div class="alert alert-warning alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <strong>{{ $message }}</strong>
                                        </div> 
                                    @enderror
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">Login</button>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            {{-- get from share all view --}}
                            {{-- @foreach (Session::get('lang_data') as $item)
                                <div class="col-md-1 col-lg-1">
                                    <a href="{{ route('lang.switch', [$item->lang ]) }}">
                                        <span class="flag-icon {{ $item->icon }}"></span>
                                    </a>
                                </div>
                            @endforeach --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('customJS')

    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/examples/sign-in.js')}}"></script>
@endsection
