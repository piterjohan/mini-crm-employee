<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompaniesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'logo', 'created_at', 'updated_at',
    ];

    public function employee()
    {
        return $this->hasMany('App\Employee','id_company');
    }
}
