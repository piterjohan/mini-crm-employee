<?php 
namespace App\ServiceClass\LoginEmployee;

use App\Models\CompaniesModel;
use Firebase\JWT\JWT;


trait LoginEmployeeTrait
{
    /**
     * Get Employee
     */
    public function getEmployee()
    {
        $company = CompaniesModel::with('employee')
            ->where('id_company', $this->user->id_company)->first();

        $this->validateEmployee($company, $company->employee);
    }

    
    /**
     * @param collection $company
     * @param collection $employee
     * if user login valid domain company
     */
    protected function validateEmployee($company, $employee)
    {
        $have_employee = $employee->pluck('email')->search($this->user->email);
        ($have_employee == 0) ? $this->validateDomain($company) : " ";
    }

    /**
     * make sure user/employee
     * login with same domain
     * @param collection $company
     */
    protected function validateDomain($company)
    {
        $url_company = $this->getFirstUrl();

        if( ucwords($url_company) == $company->name)
        {
            $this->createSessionCompany($company, $url_company);
            $this->createJWT();
        }else
        {
            $this->redirectUser();
        }
    }

    /**
     * create Session company
     * @param collection $company
     * @param string $url_company
     */
    protected function createSessionCompany($company, $url_company)
    {
        $name_company = strtolower($company->name);

        session([
            'domain_data' => [
                'url_company' => $url_company,
                'company_name' => $name_company,
                'id_company' => $company->id_company,
            ],
        ]);
    }

    /**
     * create jwt from web https://github.com/firebase/php-jwt
     * create jwt from user when login
     */
    protected function createJWT()
    {

        $key = env('KEY_JWT');
        $payload = $this->user;
    
        $jwt = JWT::encode($payload, $key);
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        session(['jwt_data' => $decoded]);
    }

    /**
     * if domain name != company->name
     * redirect to login didnt match credentials  
     */
    protected function redirectUser()
    {
        $current_url = $this->getFirstUrl();
        return route('login', $current_url);

    }

    /**
     * get first url
     * @return string 
     */
    public function getFirstUrl()
    {
        $url = url()->current();
        $current_url = explode('.', parse_url($url, PHP_URL_HOST));

        return $current_url[0];
    }
}