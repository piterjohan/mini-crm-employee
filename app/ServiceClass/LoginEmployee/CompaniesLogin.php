<?php

namespace App\ServiceClass\LoginEmployee;

use App\Models\CompaniesModel;
use Firebase\JWT\JWT;

class CompaniesLogin
{

    use LoginEmployeeTrait;

    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

}
