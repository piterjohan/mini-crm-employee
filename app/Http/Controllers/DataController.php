<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DataController extends Controller
{
    public function listEmployee()
    {        

        $employee = Employee::with('company')
                    ->where('id_company', Session::get('jwt_data')->id_company)
                    ->get();

        return view('admin-panel/show/employee-data',[
            'employees' => true,
            'list_employee_menu' => true,
            'employee' => $employee,
        ]);
    }
}
