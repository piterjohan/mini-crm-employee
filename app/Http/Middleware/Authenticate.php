<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;


class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        //parse url to get first url
        $url = url()->current();
        $current_url = explode('.', parse_url($url, PHP_URL_HOST));

        if (!$request->expectsJson()) {
            $request->session()->flush();
            return route('login', $current_url[0]);
        }
    }
}
