<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EnsureHaveJWTData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !$request->session()->get('jwt_data'))
        {
            Auth::logout();
            session(['force_logout' => 'Credential did not match at our record']);
            return redirect()->to('/login');
        }
        
        return $next($request);
    }
}
